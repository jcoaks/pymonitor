#!/usr/bin/python

import config
import subprocess
import pyrebase
import time
import urllib2
import ssl


scriptg = "scripts/restart_guacal.py"
scriptf = "scripts/restart_ferrari.py"
scriptm = "scripts/restart_mulera.py"


while True:
    try:
        gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
        firebase = pyrebase.initialize_app(config.config)
        db = firebase.database()

        print("Check")
        child = "GUACALUPDATE"
        db.child(child)
        update = db.child("restart").get()
        if update.val() == "1":
            print("Restart Guacal")
            data = {"restart": "0"}
            db.child(child).update(data)
            text_send = "Reiniciando%20Guacal"
            content = urllib2.urlopen(config.url+"&message="+text_send, context=gcontext).read()
            subprocess.call(["python", scriptg])
        time.sleep(1)
        child = "FERRARIUPDATE"
        db.child(child)
        update = db.child("restart").get()
        if update.val() == "1":
            print("Restart Ferrari")
            data = {"restart": "0"}
            db.child(child).update(data)
            text_send = "Reiniciando%20Ferrari"
            content = urllib2.urlopen(config.url+"&message="+text_send, context=gcontext).read()
            subprocess.call(["python", scriptf])
        time.sleep(1)
        child = "MULERAUPDATE"
        db.child(child)
        update = db.child("restart").get()
        if update.val() == "1":
            print("Restart Mulera")
            data = {"restart": "0"}
            db.child(child).update(data)
            text_send = "Reiniciando%20Mulera"
            content = urllib2.urlopen(config.url+"&message="+text_send, context=gcontext).read()
            subprocess.call(["python", scriptm])
        time.sleep(3)
    except: 
        time.sleep(5)