#Script de reinicio del rig conectado al raspberry pi
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(4,GPIO.OUT)
print "Presiona Boton"
GPIO.output(4,GPIO.HIGH)
time.sleep(5)
print "Rig apagado :("
GPIO.output(4,GPIO.LOW)
time.sleep(2)
print "Rig prendido :)"
GPIO.output(4,GPIO.HIGH)
time.sleep(1)
GPIO.output(4,GPIO.LOW)
