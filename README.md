# Monitor de Rigs en Python

## Instrucciones:
- pip install pyrebase

- Archivo Json de configuracion de firebase
google-service-account.json

- Archivo de configuracion
config.py

`config = {
  "apiKey": "apiKey",
  "authDomain": "projectId.firebaseapp.com",
  "databaseURL": "https://databaseName.firebaseio.com",
  "storageBucket": "projectId.appspot.com",
  "serviceAccount": "path/to/serviceAccountCredentials.json"
}`